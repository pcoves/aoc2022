mod filesystem;

use anyhow::Result;
use filesystem::FileSystem;
use std::str::FromStr;

pub fn day7() -> Result<()> {
    let file = include_str!("data");
    let filesystem = FileSystem::from_str(file)?;
    let ex0 = filesystem.ex0();
    println!("Ex0: {ex0}");

    let ex1 = filesystem.ex1();
    println!("Ex1: {ex1}");

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn exercice_0() {
        assert_eq!(
            FileSystem::from_str(include_str!("test")).unwrap().ex0(),
            95437
        );
    }

    #[test]
    fn exercice_1() {
        assert_eq!(
            FileSystem::from_str(include_str!("test")).unwrap().ex1(),
            24933642
        );
    }
}
