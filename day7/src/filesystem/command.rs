use anyhow::{bail, Error, Result};
use std::str::FromStr;

#[derive(Debug)]
pub enum Command {
    Ls,
    Cd(String),
}

impl FromStr for Command {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut words = s.split_whitespace();

        let dollar = words.next();
        let command = words.next();

        if dollar != Some("$") {
            bail!("Invalid command format: missing `$`");
        } else if command == Some("ls") {
            Ok(Command::Ls)
        } else {
            let Some(path) = words.next() else { bail!("Invalid command format: missing CD argument") };
            Ok(Command::Cd(path.to_string()))
        }
    }
}
