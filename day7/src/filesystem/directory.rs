use super::file::File;

use std::cell::RefCell;
use std::rc::{Rc, Weak};

#[derive(Debug)]
pub struct Internal {
    parent: Weak<RefCell<Self>>,
    name: String,
    directories: Vec<Rc<RefCell<Self>>>,
    files: Vec<File>,
}

impl Internal {
    pub fn size(&self) -> usize {
        self.files.iter().map(|f| f.size).sum::<usize>()
            + self
                .directories
                .iter()
                .map(|d| (d.borrow()).size())
                .sum::<usize>()
    }
}

#[derive(Debug, Clone)]
pub struct Directory(Rc<RefCell<Internal>>);

impl Directory {
    pub fn root() -> Self {
        Self(Rc::new(RefCell::new(Internal {
            parent: Weak::new(),
            name: "/".to_string(),
            directories: vec![],
            files: vec![],
        })))
    }

    pub fn directories(&self) -> Vec<Self> {
        (self.0.borrow())
            .directories
            .iter()
            .map(|i| Self(i.clone()))
            .collect()
    }

    pub fn add_directory(&self, name: String) {
        let internal = Internal {
            parent: Rc::downgrade(&self.0),
            name,
            directories: vec![],
            files: vec![],
        };

        let child = Rc::new(RefCell::new(internal));

        (self.0.borrow_mut()).directories.push(child);
    }

    pub fn add_file(&self, name: String, size: usize) {
        (self.0.borrow_mut()).files.push(File { name, size });
    }

    pub fn size(&self) -> usize {
        (self.0.borrow()).size()
    }

    pub fn parent(&self) -> Self {
        Self((self.0.borrow()).parent.upgrade().unwrap())
    }

    pub fn child(&self, name: &str) -> Self {
        Self(Rc::clone(
            &(self.0.borrow())
                .directories
                .iter()
                .find(|d| (d.borrow()).name == name)
                .expect("No such a directory"),
        ))
    }
}
