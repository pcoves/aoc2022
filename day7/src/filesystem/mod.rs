mod command;
mod directory;
mod file;

use anyhow::{bail, Error, Result};
use command::Command;
use directory::Directory;
use std::str::FromStr;

#[derive(Debug)]
pub struct FileSystem {
    root: Directory,
}

impl FileSystem {
    pub fn size(&self) -> usize {
        self.root.size()
    }

    pub fn free(&self) -> usize {
        70000000 - self.size()
    }

    pub fn ex0(&self) -> usize {
        self.directories()
            .into_iter()
            .map(|d| d.size())
            .filter(|s| *s < 100000)
            .sum()
    }

    pub fn ex1(&self) -> usize {
        let threshold = 30000000 - self.free();

        let mut sizes = self
            .directories()
            .into_iter()
            .map(|d| d.size())
            .collect::<Vec<_>>();
        sizes.sort();

        sizes.into_iter().find(|s| s > &threshold).unwrap()
    }

    fn directories(&self) -> Vec<Directory> {
        fn util(directory: &Directory, directories: &mut Vec<Directory>) {
            for directory in directory.directories() {
                directories.push(directory.clone());
                util(&directory, directories);
            }
        }

        let mut directories = vec![];
        util(&self.root, &mut directories);

        directories
    }
}

impl FromStr for FileSystem {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let root = Directory::root();
        let mut cwd = root.clone();

        let mut lines = s.lines().peekable();
        while let Some(line) = lines.next() {
            let Ok(command) = Command::from_str(line) else { bail!("Invalid input") };
            match command {
                Command::Ls => {
                    while let Some(line) = lines.peek() {
                        let mut words = line.split_whitespace();

                        let Some(word) = words.next() else { bail!("Invalid input: empty line")};
                        if word == "$" {
                            break;
                        }

                        if word == "dir" {
                            let Some(name) = words.next() else { bail!("Invalid input: missing dir name")};
                            cwd.add_directory(name.to_string());
                        } else {
                            let Ok(size) = word.parse() else { bail!("Invalid input: failed to parse file size")};
                            let Some(name) = words.next() else { bail!("Invalid input: missing file name")};
                            cwd.add_file(name.to_string(), size);
                        }

                        lines.next();
                    }
                }
                Command::Cd(path) => {
                    if path == "/" {
                        cwd = root.clone();
                    } else if path == ".." {
                        cwd = cwd.parent();
                    } else {
                        cwd = cwd.child(&path);
                    }
                }
            }
        }

        Ok(FileSystem { root })
    }
}
