#[derive(Debug)]
pub struct File {
    #[allow(dead_code)]
    pub name: String,
    pub size: usize,
}
