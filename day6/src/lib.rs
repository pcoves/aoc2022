use anyhow::{bail, Result};
use itertools::izip;
use std::collections::HashMap;

pub fn day6() -> Result<()> {
    println!("Ex0: {}", ex0(include_str!("data"))?);
    println!("Ex1: {}", ex1(include_str!("data"), 14)?);
    Ok(())
}

fn ex0(input: &str) -> Result<usize> {
    let chars0 = input.chars();
    let chars1 = chars0.clone().skip(1);
    let chars2 = chars1.clone().skip(1);
    let chars3 = chars2.clone().skip(1);

    for (i, (a, b, c, d)) in izip!(chars0, chars1, chars2, chars3).enumerate() {
        if a != b && a != c && a != d && b != c && b != d && c != d {
            return Ok(i + 4);
        }
    }

    bail!("Invalid input")
}

struct Window {
    cardinal: usize,
    map: HashMap<char, usize>,
}

impl Window {
    pub fn new(it: impl Iterator<Item = (usize, char)>) -> Self {
        let mut map = HashMap::new();
        let mut cardinal = 0;
        for (_, c) in it {
            map.entry(c).and_modify(|e| *e += 1).or_insert_with(|| {
                cardinal += 1;
                1
            });
        }
        Self { cardinal, map }
    }

    pub fn cardinal(&self) -> &usize {
        &self.cardinal
    }

    pub fn push(&mut self, c: &char) {
        self.map
            .entry(*c)
            .and_modify(|e| {
                if *e == 0 {
                    self.cardinal += 1;
                }
                *e += 1
            })
            .or_insert_with(|| {
                self.cardinal += 1;
                1
            });
    }

    pub fn pop(&mut self, c: &char) {
        self.map.entry(*c).and_modify(|e| {
            *e -= 1;
            if *e == 0 {
                self.cardinal -= 1;
            }
        });
    }
}

fn ex1(input: &str, window_size: usize) -> Result<usize> {
    let mut lhs = input.chars().enumerate();
    let mut rhs = lhs.clone();

    let mut window = Window::new(rhs.by_ref().take(window_size));

    while let (Some((i, lhs)), Some((_, rhs))) = (lhs.next(), rhs.next()) {
        window.pop(&lhs);
        window.push(&rhs);

        if *window.cardinal() == window_size {
            return Ok(i + window_size + 1);
        }
    }

    bail!("Invalid input")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ex0() -> Result<()> {
        assert_eq!(super::ex0("mjqjpqmgbljsphdztnvjfqwrcgsmlb")?, 7);
        assert_eq!(super::ex0("bvwbjplbgvbhsrlpgdmjqwftvncz")?, 5);
        assert_eq!(super::ex0("nppdvjthqldpwncqszvftbrmjlhg")?, 6);
        assert_eq!(super::ex0("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")?, 10);
        assert_eq!(super::ex0("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")?, 11);
        Ok(())
    }

    #[test]
    fn ex1() -> Result<()> {
        let ws = 14;
        assert_eq!(super::ex1("mjqjpqmgbljsphdztnvjfqwrcgsmlb", ws)?, 19);
        assert_eq!(super::ex1("bvwbjplbgvbhsrlpgdmjqwftvncz", ws)?, 23);
        assert_eq!(super::ex1("nppdvjthqldpwncqszvftbrmjlhg", ws)?, 23);
        assert_eq!(super::ex1("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", ws)?, 29);
        assert_eq!(super::ex1("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", ws)?, 26);
        Ok(())
    }
}
