#[derive(Debug, Clone)]
pub enum Operation {
    Add(usize),
    Times(usize),
    Square,
}

impl Operation {
    pub fn compute(&self, worry: usize) -> usize {
        match self {
            Self::Add(add) => worry + add,
            Self::Times(times) => worry * times,
            Self::Square => worry * worry,
        }
    }
}
