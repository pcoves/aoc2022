mod action;
mod monkey;
mod operation;

use crate::monkey::Monkey;
use anyhow::{Error, Result};
use std::str::FromStr;

#[derive(Debug)]
pub struct Day11(Vec<Monkey>, usize);

impl Day11 {
    pub fn round(&mut self) {
        for monkey in self.0.iter() {
            let operation = monkey.operation();
            let action = monkey.action();

            while let Some(item) = monkey.get() {
                let worry = operation.compute(item) % self.1;
                if worry % action.test == 0 {
                    self.0[action.success].put(worry);
                } else {
                    self.0[action.failure].put(worry);
                }
            }
        }
    }

    pub fn ex0(&self) -> usize {
        let mut inspections: Vec<usize> = self.0.iter().map(|monkey| monkey.inspection()).collect();
        inspections.sort();

        inspections.iter().rev().take(2).product()
    }
}

impl FromStr for Day11 {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut input = Vec::new();

        let mut lines = s.lines().peekable();

        while lines.peek().is_some() {
            input.push(Monkey::new(lines.by_ref())?);
            lines.next();
        }

        let div = input.iter().map(|m| m.action().test).product();

        Ok(Self(input, div))
    }
}
