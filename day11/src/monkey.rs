use crate::{action::Action, operation::Operation};
use anyhow::{bail, Result};
use std::{cell::RefCell, collections::VecDeque, iter::Peekable, str::Lines};

#[derive(Debug)]
pub struct Monkey {
    id: usize,
    items: RefCell<VecDeque<usize>>,
    inspection: RefCell<usize>,
    operation: Operation,
    action: Action,
}

impl Monkey {
    #[allow(dead_code)]
    pub fn id(&self) -> usize {
        self.id
    }

    pub fn inspection(&self) -> usize {
        self.inspection.borrow().clone()
    }

    pub fn get(&self) -> Option<usize> {
        let output = self.items.borrow_mut().pop_front();
        if output.is_some() {
            *self.inspection.borrow_mut() += 1;
        }
        output
    }

    pub fn put(&self, item: usize) {
        self.items.borrow_mut().push_back(item);
    }

    pub fn operation(&self) -> Operation {
        self.operation.clone()
    }

    pub fn action(&self) -> Action {
        self.action.clone()
    }
}

impl Monkey {
    pub fn new(lines: &mut Peekable<Lines<'_>>) -> Result<Self> {
        let Some(line) = lines.next() else { bail!("Failed to parse Monkey: missing ID line") };
        let id = {
            let Some(id) = line.split_whitespace().nth(1) else { bail!("Failed to parse Monkey: missing ID") };
            let Some(id) = id.strip_suffix(":") else { bail!("Failed to parse Monkey: ill-formed ID") };
            let Ok(id) = id.parse() else { bail!("Failed to parse Monkey: invalid ID") };
            id
        };

        let Some(line) = lines.next() else { bail!("Failed to parse Monkey: missing Items line") };
        let items = {
            let mut items = VecDeque::new();
            for item in line
                .trim()
                .split_once(":")
                .expect("Failed to parse Monkey: missing `:` separator in Items line")
                .1
                .split(",")
                .map(|s| s.trim())
            {
                items.push_back(item.parse()?);
            }
            RefCell::new(items)
        };

        let Some(line) = lines.next() else { bail!("Failed to parse Monkey: missing Operation line") };
        let operation = {
            let mut words = line
                .trim()
                .split_once("=")
                .expect("Failed to parse Monkey: missing `=` separator in Operation line")
                .1
                .split_whitespace()
                .skip(1);

            let Some(operator) = words.next() else { bail!("Failed to parse Monkey: missing operator")};
            let Some(operand) = words.next() else { bail!("Failed to parse Monkey: missing operand")};

            if operator == "+" {
                let Ok(operand) = operand.parse() else { bail!("Failed to parse Monkey: invalid operand")};
                Operation::Add(operand)
            } else {
                if operand == "old" {
                    Operation::Square
                } else {
                    let Ok(operand) = operand.parse() else { bail!("Failed to parse Monkey: invalid operand")};
                    Operation::Times(operand)
                }
            }
        };

        let action = {
            let Some(test) = lines.next() else { bail!("Failed to parse Monkey: missing Test line")};
            let Some(success) = lines.next() else { bail!("Failed to parse Monkey: missing Success line")};
            let Some(failure) = lines.next() else { bail!("Failed to parse Monkey: missing Failure line")};

            let test = {
                let Some(test) = test.split_whitespace().last() else { bail!("Failed to parse Monkey: missing Test value") };
                let Ok(test) = test.parse() else { bail!("Failed to parse Monkey: invalid Test value") };
                test
            };

            let success = {
                let Some(success) = success.split_whitespace().last() else { bail!("Failed to parse Monkey: missing Success value") };
                let Ok(success) = success.parse() else { bail!("Failed to parse Monkey: invalid Success value") };
                success
            };

            let failure = {
                let Some(failure) = failure.split_whitespace().last() else { bail!("Failed to parse Monkey: missing Failure value") };
                let Ok(failure) = failure.parse() else { bail!("Failed to parse Monkey: invalid Failure value") };
                failure
            };

            Action {
                test,
                success,
                failure,
            }
        };

        Ok(Self {
            id,
            items,
            inspection: RefCell::new(0),
            operation,
            action,
        })
    }
}
