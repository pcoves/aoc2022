#[derive(Debug, Clone)]
pub struct Action {
    pub test: usize,
    pub success: usize,
    pub failure: usize,
}
