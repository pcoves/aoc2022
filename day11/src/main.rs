use anyhow::Result;
use day11::Day11;

fn main() -> Result<()> {
    let mut day11: Day11 = include_str!("data").parse()?;
    for _ in 0..10000 {
        day11.round();
    }
    println!("Ex0: {}", day11.ex0());

    Ok(())
}
