use anyhow::Result;
use day1::day1;
use day2::day2;
use day3::day3;
use day4::day4;
use day5::day5;
use day6::day6;
use day7::day7;
use day8::day8;

fn main() -> Result<()> {
    day1()?;
    day2()?;
    day3()?;
    day4()?;
    day5()?;
    day6()?;
    day7()?;
    day8()?;
    Ok(())
}
