use std::{fmt::Display, iter::Peekable, slice::Iter};

use crate::input::{Input, Instruction};

#[derive(Debug)]
pub struct Device<'a> {
    x: isize,
    cycle: isize,
    wait: bool,
    current: Option<&'a Instruction>,
    instructions: Peekable<Iter<'a, Instruction>>,
    crt: [bool; 40 * 6],
}

impl<'a> Device<'a> {
    pub fn new(input: &'a Input) -> Self {
        Self {
            x: 1,
            cycle: 0,
            wait: true,
            current: None,
            instructions: input.iter().peekable(),
            crt: [false; 40 * 6],
        }
    }

    pub fn ticks(&mut self, ticks: isize) -> isize {
        for _ in 0..(ticks - 1) {
            self.tick();
        }
        self.tick()
    }

    fn tick(&mut self) -> isize {
        match self.current {
            None | Some(Instruction::Noop) => {
                self.current = self.instructions.next();
            }
            Some(Instruction::Addx(increment)) => {
                if self.wait {
                    self.wait = false;
                } else {
                    self.x += increment;
                    self.current = self.instructions.next();
                    self.wait = true;
                }
            }
        }

        if (self.cycle % 40) - 1 <= self.x && self.x <= (self.cycle % 40) + 1 {
            self.crt[self.cycle as usize] = true;
        }

        self.cycle += 1;
        self.cycle * self.x
    }

    pub fn ex1(&mut self) {
        while self.instructions.peek().is_some() {
            self.ticks(1);
        }
    }
}

impl<'a> Display for Device<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for h in 0..6 {
            for w in 0..40 {
                if self.crt[h * 40 + w] {
                    write!(f, "#")
                } else {
                    write!(f, ".")
                }?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}
