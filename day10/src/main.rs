use anyhow::Result;
use day10::{ex0, ex1};

fn main() -> Result<()> {
    println!("Ex0: {}", ex0(include_str!("data"))?);
    ex1(include_str!("data"));
    Ok(())
}
