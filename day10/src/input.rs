use anyhow::{bail, Error, Result};
use std::{ops::Deref, str::FromStr};

pub struct Input(Vec<Instruction>);

impl Deref for Input {
    type Target = Vec<Instruction>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl FromStr for Input {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut input = vec![];

        for line in s.lines() {
            input.push(line.parse()?);
        }

        Ok(Self(input))
    }
}

#[derive(Debug)]
pub enum Instruction {
    Noop,
    Addx(isize),
}

impl FromStr for Instruction {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut words = s.split_whitespace();

        let Some(instruction) = words.next() else { bail!("Failed to parse Instruction: empty") };

        Ok(match instruction {
            "noop" => Self::Noop,
            "addx" => {
                let Some(increment) = words.next() else { bail!("Failed to parse Instruction: missing increment") };
                let Ok(increment) = increment.parse() else { bail!("Failed to parse Instruction:: invalid increment") };
                Self::Addx(increment)
            }
            _ => bail!("Failed to parse Instruction: unknown instruction"),
        })
    }
}
