mod device;
mod input;

use crate::{device::Device, input::Input};
use anyhow::Result;

pub fn ex0(input: &str) -> Result<isize> {
    let input = input.parse::<Input>()?;

    let mut device = Device::new(&input);

    let mut score = device.ticks(20);
    for _ in 1..6 {
        score += device.ticks(40);
    }

    Ok(score)
}

pub fn ex1(input: &str) {
    let input = input.parse::<Input>().unwrap();
    let mut device = Device::new(&input);
    device.ex1();

    println!("{device}");
}
