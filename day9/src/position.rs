#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct P(i32, i32);

impl P {
    pub fn new() -> Self {
        Self(0, 0)
    }

    pub fn up(&mut self) {
        self.0 -= 1;
    }
    pub fn down(&mut self) {
        self.0 += 1;
    }
    pub fn left(&mut self) {
        self.1 -= 1;
    }
    pub fn right(&mut self) {
        self.1 += 1;
    }

    pub fn follow(&mut self, target: &Self) {
        let distance = self.distance(target);
        if distance != 0 {
            if distance == 2 {
                if target.0 - self.0 == 0 {
                    let vertical = target.1 - self.1;
                    self.1 += vertical / vertical.abs();
                } else if target.1 - self.1 == 0 {
                    let horizontal = target.0 - self.0;
                    self.0 += horizontal / horizontal.abs();
                }
            } else if distance >= 3 {
                let horyzontal = target.0 - self.0;
                let vertical = target.1 - self.1;

                self.0 += horyzontal / horyzontal.abs();
                self.1 += vertical / vertical.abs();
            }
        }
    }

    pub fn distance(&self, other: &Self) -> i32 {
        (self.0 - other.0).abs() + (self.1 - other.1).abs()
    }
}
