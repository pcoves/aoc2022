use anyhow::Result;
use day9::Input;

fn main() -> Result<()> {
    let input = include_str!("data");
    println!("Ex0 {}", input.parse::<Input<2>>()?.visited());
    println!("Ex1 {}", input.parse::<Input<10>>()?.visited());
    Ok(())
}
