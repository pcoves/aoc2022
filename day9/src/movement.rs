use anyhow::{bail, Error, Result};
use std::str::FromStr;

#[derive(Debug, Clone)]
pub enum Movement {
    Up(u8),
    Down(u8),
    Left(u8),
    Right(u8),
}

impl FromStr for Movement {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut words = s.split_whitespace();

        let Some(direction) = words.next() else { bail!("Failed to parse Move: missing direction") };

        let Some(distance) = words.next() else { bail!("Failed to parse Move: missing distance") };
        let Ok(distance) = distance.parse() else { bail!("Failed to parse Move: invalaid distance") };

        Ok(match direction {
            "U" => Self::Up(distance),
            "D" => Self::Down(distance),
            "L" => Self::Left(distance),
            "R" => Self::Right(distance),
            _ => panic!("Failed to parse Move: invalid direction"),
        })
    }
}
