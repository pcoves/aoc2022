mod movement;
mod position;

use crate::{movement::Movement, position::P};
use anyhow::{Error, Result};
use std::collections::HashSet;
use std::str::FromStr;

#[derive(Debug)]
pub struct Input<const L: usize> {
    rope: [P; L],
    visited: HashSet<P>,
}

impl<const L: usize> Input<L> {
    pub fn new() -> Self {
        let mut visited = HashSet::new();
        visited.insert(P::new());

        Self {
            rope: [P::new(); L],
            visited,
        }
    }

    pub fn visited(&self) -> usize {
        self.visited.len()
    }

    fn util(&mut self) {
        let mut positions = self.rope.iter_mut().rev();
        let Some(mut target) = positions.next() else { panic!("Invalid rope's length") };
        while let Some(position) = positions.next() {
            position.follow(target);
            target = position;
        }
    }

    pub fn up(&mut self, distance: u8) {
        for _ in 0..distance {
            self.rope.last_mut().map(|p| p.up());
            self.util();
            self.visited.insert(self.rope[0]);
        }
    }

    pub fn down(&mut self, distance: u8) {
        for _ in 0..distance {
            self.rope.last_mut().map(|p| p.down());
            self.util();
            self.visited.insert(self.rope[0]);
        }
    }

    pub fn left(&mut self, distance: u8) {
        for _ in 0..distance {
            self.rope.last_mut().map(|p| p.left());
            self.util();
            self.visited.insert(self.rope[0]);
        }
    }

    pub fn right(&mut self, distance: u8) {
        for _ in 0..distance {
            self.rope.last_mut().map(|p| p.right());
            self.util();
            self.visited.insert(self.rope[0]);
        }
    }
}

impl<const L: usize> FromStr for Input<L> {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut input = Input::new();

        for line in s.lines() {
            match line.parse()? {
                Movement::Up(distance) => input.up(distance),
                Movement::Down(distance) => input.down(distance),
                Movement::Left(distance) => input.left(distance),
                Movement::Right(distance) => input.right(distance),
            }
        }

        Ok(input)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        assert_eq!(ex0(include_str!("test")).unwrap(), 13);
        assert_eq!(ex1(include_str!("test")).unwrap(), 1);
        assert_eq!(ex1(include_str!("test2")).unwrap(), 36);
    }
}
