use anyhow::{Error, Result};
use lazy_static::lazy_static;
use regex::Regex;
use std::str::FromStr;

fn main() {
    let file = include_str!("data").parse::<File>().unwrap();
    println!("Ex0: {}", file.ex0(2000000));
    println!("Ex1: {}", file.ex1(4000000).unwrap());
}

#[derive(Debug)]
struct File {
    min: Position,
    max: Position,
    data: Vec<Line>,
}

impl File {
    pub fn ex0(&self, index: isize) -> usize {
        let mut score = 0;

        for col in self.min.0..(self.max.0 + 1) {
            for line in &self.data {
                let position = Position(col, index);

                if line.contains(&position) && !self.overlap(&position) {
                    score += 1;
                    break;
                }
            }
        }
        score
    }

    pub fn ex1(&self, max: isize) -> Option<isize> {
        for line in &self.data {
            'position: for position in line.borders(max) {
                for line in &self.data {
                    if line.contains(&position) {
                        continue 'position;
                    }
                }
                return Some(position.0 * 4000000 + position.1);
            }
        }
        None
    }

    fn overlap(&self, position: &Position) -> bool {
        for line in &self.data {
            if line.sensor == *position || line.closest_beacon == *position {
                return true;
            }
        }

        false
    }
}

#[derive(Debug)]
struct Line {
    sensor: Position,
    closest_beacon: Position,
}

impl Line {
    pub fn contains(&self, position: &Position) -> bool {
        self.sensor.distance(position) <= self.radius()
    }

    fn radius(&self) -> usize {
        self.sensor.distance(&self.closest_beacon)
    }

    fn borders(&self, max: isize) -> Vec<Position> {
        let radius = self.radius() as isize;

        let mut positions = vec![];
        let position = Position(self.sensor.0 + radius + 1, self.sensor.1);
        if position.0 >= 0 && position.0 <= max && position.1 >= 0 && position.1 <= max {
            positions.push(position);
        }
        let position = Position(self.sensor.0 - radius - 1, self.sensor.1);
        if position.0 >= 0 && position.0 <= max && position.1 >= 0 && position.1 <= max {
            positions.push(position);
        }
        let position = Position(self.sensor.0, self.sensor.1 + radius + 1);
        if position.0 >= 0 && position.0 <= max && position.1 >= 0 && position.1 <= max {
            positions.push(position);
        }
        let position = Position(self.sensor.0, self.sensor.1 - radius - 1);
        if position.0 >= 0 && position.0 <= max && position.1 >= 0 && position.1 <= max {
            positions.push(position);
        }
        for i in 1..radius + 1 {
            {
                let position = Position(self.sensor.0 + radius + 1 - i, self.sensor.1 - i);
                if position.0 >= 0 && position.0 <= max && position.1 >= 0 && position.1 <= max {
                    positions.push(position);
                }
            }
            {
                let position = Position(self.sensor.0 + radius + 1 - i, self.sensor.1 + i);
                if position.0 >= 0 && position.0 <= max && position.1 >= 0 && position.1 <= max {
                    positions.push(position);
                }
            }
            {
                let position = Position(self.sensor.0 - radius - 1 + i, self.sensor.1 - i);
                if position.0 >= 0 && position.0 <= max && position.1 >= 0 && position.1 <= max {
                    positions.push(position);
                }
            }
            {
                let position = Position(self.sensor.0 - radius - 1 + i, self.sensor.1 + i);
                if position.0 >= 0 && position.0 <= max && position.1 >= 0 && position.1 <= max {
                    positions.push(position);
                }
            }
        }
        positions
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Position(isize, isize);

impl Position {
    pub fn distance(&self, position: &Position) -> usize {
        self.0.abs_diff(position.0) + self.1.abs_diff(position.1)
    }

    pub fn min() -> Self {
        Self(isize::MAX, isize::MAX)
    }

    pub fn max() -> Self {
        Self(isize::MIN, isize::MIN)
    }
}

impl FromStr for File {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut data = vec![];
        let mut min = Position::min();
        let mut max = Position::max();

        for line in s.lines() {
            let line = line.parse::<Line>()?;
            let radius = line.radius();

            min.0 = min.0.min(line.sensor.0 - radius as isize);
            min.1 = min.1.min(line.sensor.1 - radius as isize);
            max.0 = max.0.max(line.sensor.0 + radius as isize);
            max.1 = max.1.max(line.sensor.1 + radius as isize);

            data.push(line)
        }

        Ok(Self { data, min, max })
    }
}

impl FromStr for Line {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref REGEX: Regex = Regex::new(
                r"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)"
            )
            .unwrap();
        }

        let captures = REGEX.captures(s).expect("Failed to parse line");
        let sensor = Position(captures[1].parse()?, captures[2].parse()?);
        let closest_beacon = Position(captures[3].parse()?, captures[4].parse()?);

        Ok(Self {
            sensor,
            closest_beacon,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ex0() -> Result<()> {
        assert_eq!(include_str!("test").parse::<File>()?.ex0(10), 26);
        assert_eq!(
            include_str!("test").parse::<File>()?.ex1(20).unwrap(),
            56000011
        );
        Ok(())
    }
}
