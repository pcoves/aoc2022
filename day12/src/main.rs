use std::{
    cell::RefCell,
    collections::HashMap,
    rc::{Rc, Weak},
    str::FromStr,
    vec,
};

use anyhow::{Error, Result};

fn main() {
    let input = include_str!("data")
        .parse::<Input>()
        .expect("Failed to parse input");
    let graph = Graph::new(&input, input.end());
    println!("Ex0: {}", graph.dijkstra(input.start()).unwrap());
    println!(
        "Ex1: {}",
        input
            .starts()
            .into_iter()
            .filter_map(|s| graph.dijkstra(s))
            .min()
            .unwrap()
    );
}

#[derive(Debug)]
struct Input {
    height: usize,
    width: usize,
    data: Vec<char>,
}

impl Input {
    pub fn height(&self) -> usize {
        self.height
    }

    pub fn width(&self) -> usize {
        self.width
    }

    pub fn get(&self, row: usize, col: usize) -> Option<u32> {
        self.data.get(row * self.width + col).map(|c| match *c {
            'S' => 0,
            'E' => 25,
            _ => *c as u32 - 'a' as u32,
        })
    }

    pub fn start(&self) -> Coordinate {
        let start = self.data.iter().position(|c| *c == 'S').unwrap();
        Coordinate(start / self.width, start % self.width)
    }

    pub fn end(&self) -> Coordinate {
        let start = self.data.iter().position(|c| *c == 'E').unwrap();
        Coordinate(start / self.width, start % self.width)
    }

    pub fn starts(&self) -> Vec<Coordinate> {
        self.data
            .iter()
            .enumerate()
            .filter_map(|(index, letter)| {
                if *letter == 'a' {
                    Some(Coordinate(index / self.width(), index % self.width()))
                } else {
                    None
                }
            })
            .collect()
    }
}

impl FromStr for Input {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut height = 0;
        let mut data = vec![];

        for l in s.lines() {
            data.append(&mut l.chars().collect());
            height += 1;
        }

        Ok(Self {
            height,
            width: data.len() / height,
            data,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct Coordinate(usize, usize);

impl Coordinate {
    pub fn top(&self) -> Self {
        Self(self.0 - 1, self.1)
    }

    pub fn bottom(&self) -> Self {
        Self(self.0 + 1, self.1)
    }

    pub fn left(&self) -> Self {
        Self(self.0, self.1 - 1)
    }

    pub fn right(&self) -> Self {
        Self(self.0, self.1 + 1)
    }
}

#[derive(Debug)]
struct Graph {
    end: Coordinate,
    nodes: HashMap<Coordinate, Node>,
    arcs: HashMap<Coordinate, Vec<Arc>>,
}

impl Graph {
    pub fn new(input: &Input, end: Coordinate) -> Self {
        let mut nodes = HashMap::new();
        for row in 0..input.height() {
            for col in 0..input.width() {
                nodes.insert(
                    Coordinate(row, col),
                    Node::new(Coordinate(row, col), input.get(row, col).unwrap()),
                );
            }
        }

        let mut arcs = HashMap::new();
        for row in 0..input.height() {
            for col in 0..input.width() {
                let coordinates = Coordinate(row, col);
                let source = nodes.get(&coordinates).unwrap();

                let mut neighbors = vec![];

                if row != 0 {
                    if let Some(top) = nodes.get(&coordinates.top()) {
                        if top.height() <= source.height() + 1 {
                            neighbors.push(Arc {
                                source: Rc::downgrade(&source.0),
                                target: Rc::downgrade(&top.0),
                            })
                        }
                    }
                }

                if let Some(bottom) = nodes.get(&coordinates.bottom()) {
                    if bottom.height() <= source.height() + 1 {
                        neighbors.push(Arc {
                            source: Rc::downgrade(&source.0),
                            target: Rc::downgrade(&bottom.0),
                        })
                    }
                }

                if col != 0 {
                    if let Some(left) = nodes.get(&coordinates.left()) {
                        if left.height() <= source.height() + 1 {
                            neighbors.push(Arc {
                                source: Rc::downgrade(&source.0),
                                target: Rc::downgrade(&left.0),
                            })
                        }
                    }
                }

                if let Some(right) = nodes.get(&coordinates.right()) {
                    if right.height() <= source.height() + 1 {
                        neighbors.push(Arc {
                            source: Rc::downgrade(&source.0),
                            target: Rc::downgrade(&right.0),
                        })
                    }
                }

                arcs.insert(coordinates, neighbors);
            }
        }

        Self { end, nodes, arcs }
    }

    pub fn dijkstra(&self, start: Coordinate) -> Option<usize> {
        let mut to_visit = self.to_visit(start);

        while !to_visit.is_empty() {
            to_visit.sort_by(|lhs, rhs| rhs.cmp(lhs));

            let current = to_visit.pop().unwrap();

            let coordinates = current.coordinates();
            let distance = current.distance() + 1;

            for arc in self.arcs.get(&coordinates).unwrap() {
                let target = arc.target.upgrade().unwrap();
                if target.borrow().coordinates == self.end {
                    return Some(distance);
                }

                if distance < target.borrow().distance {
                    target.borrow_mut().distance = distance;
                    let node = Node(target);
                    to_visit.push(node);
                }
            }

            // for (_, n) in &to_visit {
            //     println!("{}", n.distance());
            // }
            // break 32;
        }
        None
    }

    fn to_visit(&self, start: Coordinate) -> Vec<Node> {
        let node = self.nodes.get(&start).unwrap().clone();
        node.0.borrow_mut().distance = 0;
        vec![node]
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
struct Node(Rc<RefCell<Node_>>);

impl Node {
    pub fn new(coordinates: Coordinate, height: u32) -> Self {
        Node(Rc::new(RefCell::new(Node_ {
            coordinates,
            height,
            distance: usize::MAX,
        })))
    }

    pub fn coordinates(&self) -> Coordinate {
        self.0.borrow().coordinates.clone()
    }

    pub fn height(&self) -> u32 {
        self.0.borrow().height
    }

    pub fn distance(&self) -> usize {
        self.0.borrow().distance
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Node_ {
    coordinates: Coordinate,
    height: u32,
    distance: usize,
}

impl PartialOrd for Node_ {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.distance.partial_cmp(&other.distance)
    }
}

impl Ord for Node_ {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.partial_cmp(&other).unwrap()
    }
}

#[derive(Debug)]
struct Arc {
    #[allow(dead_code)]
    source: Weak<RefCell<Node_>>,
    target: Weak<RefCell<Node_>>,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() -> Result<()> {
        let input = &include_str!("test").parse::<Input>()?;
        let graph = Graph::new(&input, input.end());
        assert_eq!(graph.dijkstra(input.start()).unwrap(), 31);
        assert_eq!(
            input
                .starts()
                .into_iter()
                .filter_map(|s| graph.dijkstra(s))
                .min()
                .unwrap(),
            29
        );
        Ok(())
    }
}
