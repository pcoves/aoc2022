use anyhow::{Error, Result};
use std::str::FromStr;

pub fn day3() -> Result<()> {
    ex0()?;
    ex1()
}

fn ex0() -> Result<()> {
    println!("Ex0: {}", Rucksacks::from_str(include_str!("data"))?.ex0());
    Ok(())
}

fn ex1() -> Result<()> {
    println!("Ex0: {}", Rucksacks::from_str(include_str!("data"))?.ex1());
    Ok(())
}

#[derive(Debug)]
struct Rucksacks(Vec<Rucksack>);

#[derive(Debug)]
struct Rucksack(String, String);

impl FromStr for Rucksacks {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut rucksacks = vec![];
        for line in s.lines() {
            rucksacks.push(Rucksack::from_str(line)?);
        }
        Ok(Self(rucksacks))
    }
}

impl FromStr for Rucksack {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (lhs, rhs) = s.split_at(s.len() / 2);
        Ok(Self(lhs.to_string(), rhs.to_string()))
    }
}

impl Rucksacks {
    pub fn ex0(&self) -> u32 {
        self.0.iter().map(|r| r.priority().unwrap()).sum()
    }

    pub fn ex1(&self) -> u32 {
        Groups::from(self)
            .0
            .iter()
            .map(|g| g.priority().unwrap())
            .sum()
    }
}

impl Rucksack {
    pub fn priority(&self) -> Option<u32> {
        self.common().map(|c| c.into())
    }

    fn common(&self) -> Option<Common> {
        for c in self.0.chars() {
            if self.1.contains(c) {
                return Some(Common(c));
            }
        }
        None
    }

    pub fn contains(&self, c: &char) -> bool {
        self.0.contains(*c) || self.1.contains(*c)
    }
}

#[derive(Debug)]
struct Groups<'a>(Vec<Group<'a>>);

#[derive(Debug)]
struct Group<'a>(&'a Rucksack, &'a Rucksack, &'a Rucksack);

impl<'a> From<&'a Rucksacks> for Groups<'a> {
    fn from(rucksacks: &'a Rucksacks) -> Self {
        let mut groups = vec![];

        for chunks in rucksacks.0.chunks(3).into_iter() {
            groups.push(Group(&chunks[0], &chunks[1], &chunks[2]));
        }

        Self(groups)
    }
}

impl<'a> Group<'a> {
    pub fn priority(&self) -> Option<u32> {
        self.common().map(|c| c.into())
    }

    pub fn common(&self) -> Option<Common> {
        for c in self.0 .0.chars() {
            if self.1.contains(&c) && self.2.contains(&c) {
                return Some(Common(c));
            }
        }
        for c in self.0 .1.chars() {
            if self.1.contains(&c) && self.2.contains(&c) {
                return Some(Common(c));
            }
        }
        None
    }
}

#[derive(Debug)]
struct Common(char);

impl Into<u32> for Common {
    fn into(self) -> u32 {
        if self.0.is_lowercase() {
            self.0 as u32 - 'a' as u32 + 1
        } else {
            self.0 as u32 - 'A' as u32 + 26 + 1
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn exercice_0() -> Result<()> {
        assert_eq!(Rucksacks::from_str(include_str!("test"))?.ex0(), 157);
        Ok(())
    }

    #[test]
    fn exercice_1() -> Result<()> {
        assert_eq!(Rucksacks::from_str(include_str!("test"))?.ex1(), 70);
        Ok(())
    }
}
