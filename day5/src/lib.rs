// Disclamer: Day5 was a pain in the parse.

use anyhow::{Error, Result};
use std::{
    collections::{HashMap, LinkedList},
    fmt::Display,
    str::FromStr,
};

pub fn day5() -> Result<()> {
    println!("Ex0: {}", ex0(include_str!("data"))?);
    println!("Ex1: {}", ex1(include_str!("data"))?);
    Ok(())
}

fn ex0(file: &str) -> Result<String> {
    Ok(Input::from_str(file)?.ex0().to_string())
}

fn ex1(file: &str) -> Result<String> {
    Ok(Input::from_str(file)?.ex1().to_string())
}

#[derive(Debug)]
struct Input {
    stacks: Stacks,
    moves: Moves,
}

impl FromStr for Input {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut lines = s.lines().filter(|s| !s.is_empty());

        let stacks = 'stacks: {
            let mut stacks = HashMap::new();
            while let Some(line) = lines.next() {
                for (i, c) in line.chars().enumerate() {
                    match i % 4 {
                        0 => {
                            if c.is_whitespace() || c == '[' {
                                continue;
                            } else {
                                break 'stacks stacks;
                            }
                        }
                        1 => {
                            if c.is_whitespace() {
                                continue;
                            } else if c.is_ascii_digit() {
                                break 'stacks stacks;
                            } else {
                                stacks
                                    .entry(i / 4 + i % 4)
                                    .or_insert(LinkedList::new())
                                    .push_front(c);
                            }
                        }
                        2 => {
                            if c.is_whitespace() || c == ']' {
                                continue;
                            } else {
                                break 'stacks stacks;
                            }
                        }
                        3 => {
                            if c.is_whitespace() {
                                continue;
                            } else {
                                break 'stacks stacks;
                            }
                        }
                        _ => {
                            panic!("This really can't happen");
                        }
                    }
                }
            }
            stacks
        };

        let moves = {
            let mut moves = vec![];
            while let Some(line) = lines.next() {
                let words = line.split_whitespace().collect::<Vec<_>>();
                moves.push(Move {
                    cardinal: words[1].parse()?,
                    source: words[3].parse()?,
                    target: words[5].parse()?,
                });
            }
            moves
        };

        Ok(Self {
            stacks: Stacks(stacks),
            moves: Moves(moves),
        })
    }
}

impl Input {
    fn ex0(mut self) -> Self {
        for Move {
            cardinal,
            source,
            target,
        } in self.moves.0.iter()
        {
            for _ in 0..*cardinal {
                let source = self.stacks.0.get_mut(source).unwrap().pop_back().unwrap();
                self.stacks.0.get_mut(target).unwrap().push_back(source);
            }
        }

        self
    }

    fn ex1(mut self) -> Self {
        for Move {
            cardinal,
            source,
            target,
        } in self.moves.0.iter()
        {
            let source = self.stacks.0.get_mut(source).unwrap();
            let mut tmp = source.split_off(source.len() - cardinal);
            self.stacks.0.get_mut(target).unwrap().append(&mut tmp);
        }

        self
    }
}

impl Display for Input {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut stacks = self.stacks.0.iter().collect::<Vec<_>>();
        stacks.sort();

        for (_, s) in stacks.iter_mut() {
            write!(f, "{}", *s.back().unwrap())?;
        }

        Ok(())
    }
}

#[derive(Debug)]
struct Stacks(HashMap<usize, LinkedList<char>>);

#[derive(Debug)]
struct Moves(Vec<Move>);

#[derive(Debug)]
struct Move {
    cardinal: usize,
    source: usize,
    target: usize,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn exercice_0() {
        assert_eq!(ex0(include_str!("test")).unwrap(), "CMZ".to_owned());
    }
}
