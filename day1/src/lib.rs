use anyhow::{Error, Result};

use std::{iter::Sum, ops::Add, str::FromStr};

pub fn day1() -> Result<()> {
    exercice_0()?;
    exercice_1()?;
    Ok(())
}

fn exercice_0() -> Result<()> {
    println!(
        "Ex01, {:?}",
        Expedition::from_str(include_str!("data"))?.ex0()
    );
    Ok(())
}

fn exercice_1() -> Result<()> {
    println!(
        "Ex01, {:?}",
        Expedition::from_str(include_str!("data"))?.ex1()
    );
    Ok(())
}

#[derive(Debug)]
struct Expedition(Vec<Elf>);

#[derive(Debug)]
struct Elf(Vec<Calories>);

#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Ord)]
struct Calories(u32);

impl Expedition {
    pub fn ex0(self) -> Calories {
        self.0.into_iter().map(|e| e.calories()).max().unwrap()
    }

    pub fn ex1(self) -> Calories {
        let mut calories = self.0.into_iter().map(|e| e.calories()).collect::<Vec<_>>();
        calories.sort_by(|lhs, rhs| rhs.partial_cmp(lhs).unwrap());
        calories.into_iter().take(3).sum()
    }
}

impl FromStr for Expedition {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut expedition: Vec<Elf> = vec![];

        let mut lines = s.lines().peekable();
        while let Some(line) = lines.next() {
            let mut elf = vec![Calories::from_str(line)?];

            while let Some(line) = lines.next() {
                match Calories::from_str(line) {
                    Ok(calories) => elf.push(calories),
                    Err(_) => break,
                }
            }
            expedition.push(Elf(elf))
        }

        Ok(Expedition(expedition))
    }
}

impl Elf {
    pub fn calories(self) -> Calories {
        self.0.into_iter().sum()
    }
}

impl FromStr for Calories {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        Ok(Calories(s.parse::<u32>()?))
    }
}

impl Add for Calories {
    type Output = Self;

    fn add(self, Calories(rhs): Calories) -> Self {
        Self(self.0 + rhs)
    }
}

impl Sum for Calories {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.reduce(|acc, val| acc + val).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn exercice_0() -> Result<()> {
        assert_eq!(
            Expedition::from_str(include_str!("test"))?.ex0(),
            Calories(24000)
        );
        Ok(())
    }

    #[test]
    fn exercice_1() -> Result<()> {
        assert_eq!(
            Expedition::from_str(include_str!("test"))?.ex1(),
            Calories(45000)
        );
        Ok(())
    }
}
