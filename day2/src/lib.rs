use anyhow::{Error, Result};
use std::{iter::Sum, ops::Add, str::FromStr};
use Move::*;

pub fn day2() -> Result<()> {
    exercice_0()?;
    exercice_1()?;
    Ok(())
}

fn exercice_0() -> Result<()> {
    println!("Ex1, {:?}", Game::from_str(include_str!("data"))?.ex0());
    Ok(())
}

fn exercice_1() -> Result<()> {
    println!("Ex0, {:?}", Game::from_str(include_str!("data"))?.ex1());
    Ok(())
}

struct Game(Vec<Round>);

impl Game {
    pub fn ex0(self) -> Score {
        self.0.into_iter().map(|r| r.score()).sum()
    }

    pub fn ex1(self) -> Score {
        self.0.into_iter().map(|r| r.compute().score()).sum()
    }
}

impl FromStr for Game {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut game = vec![];

        for line in s.lines() {
            game.push(Round::from_str(line).expect("Failed to pare Round"));
        }

        Ok(Game(game))
    }
}

struct Round(Move, Move);

impl Round {
    pub fn score(&self) -> Score {
        Score(match (&self.0, &self.1) {
            (Rock, Rock) => 1 + 3,
            (Rock, Paper) => 2 + 6,
            (Rock, Scisors) => 3 + 0,
            (Paper, Rock) => 1 + 0,
            (Paper, Paper) => 2 + 3,
            (Paper, Scisors) => 3 + 6,
            (Scisors, Rock) => 1 + 6,
            (Scisors, Paper) => 2 + 0,
            (Scisors, Scisors) => 3 + 3,
        })
    }

    pub fn compute(&self) -> Self {
        match (&self.0, &self.1) {
            (Rock, Rock) => Self(Rock, Scisors),
            (Rock, Paper) => Self(Rock, Rock),
            (Rock, Scisors) => Self(Rock, Paper),
            (Paper, Rock) => Self(Paper, Rock),
            (Paper, Paper) => Self(Paper, Paper),
            (Paper, Scisors) => Self(Paper, Scisors),
            (Scisors, Rock) => Self(Scisors, Paper),
            (Scisors, Paper) => Self(Scisors, Scisors),
            (Scisors, Scisors) => Self(Scisors, Rock),
        }
    }
}

#[derive(strum::EnumString)]
enum Move {
    #[strum(serialize = "A", serialize = "X")]
    Rock,

    #[strum(serialize = "B", serialize = "Y")]
    Paper,

    #[strum(serialize = "C", serialize = "Z")]
    Scisors,
}

#[derive(Debug, PartialEq)]
struct Score(u32);

impl Add for Score {
    type Output = Self;

    fn add(self, Score(rhs): Score) -> Self {
        Self(self.0 + rhs)
    }
}

impl Sum for Score {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.reduce(|acc, val| acc + val).unwrap()
    }
}

impl FromStr for Round {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut split = s.split_whitespace();

        let player0 = split.next().expect("No Player0 on this line");
        let player1 = split.next().expect("No Player1 on this line");

        Ok(Round(
            Move::from_str(player0).expect("Failed to parse Player0"),
            Move::from_str(player1).expect("Failed to parse Player1"),
        ))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn exercice_0() -> Result<()> {
        assert_eq!(Game::from_str(include_str!("test"))?.ex0(), Score(15));
        Ok(())
    }

    #[test]
    fn exercice_1() -> Result<()> {
        assert_eq!(Game::from_str(include_str!("test"))?.ex1(), Score(12));
        Ok(())
    }
}
