// Misread the puzzle today.
// Solution works but is badly implemented.
// Might come back on this later.

use anyhow::{anyhow, Error, Result};
use std::{
    fmt::Display,
    iter::{Rev, Skip, StepBy, Take},
    slice::IterMut,
    str::FromStr,
};

pub fn day8() -> Result<()> {
    let mut input = Input::from_str(include_str!("data"))?;
    println!("Ex0: {}", input.ex0());
    println!("Ex1: {}", input.ex1());
    Ok(())
}

#[derive(Debug)]
struct Input {
    width: usize,
    height: usize,
    data: Vec<(u32, bool, usize)>,
}

impl Input {
    pub fn ex0(&mut self) -> usize {
        for index in 0..self.height {
            self.from_left(index).ex0();
            self.from_right(index).ex0();
        }

        for index in 0..self.width {
            self.from_top(index).ex0();
            self.from_bottom(index).ex0();
        }

        for index in 0..self.width {
            self.data[index].1 = true;
            self.data[self.width * (self.height - 1) + index].1 = true;
        }

        for index in 0..self.height {
            self.data[self.width * index].1 = true;
            self.data[self.width * index + (self.width - 1)].1 = true;
        }

        self.data
            .iter()
            .filter(|(_, v, _)| *v)
            .collect::<Vec<_>>()
            .len()
    }

    pub fn ex1(&mut self) -> usize {
        for i in 1..(self.height - 1) {
            for j in 1..(self.width - 1) {
                let (size, _, _) = self.get(i, j);

                let to_the_right = {
                    let mut score = 0;
                    for k in (j + 1)..self.width {
                        let (s, _, _) = self.get(i, k);
                        if s < size {
                            score += 1;
                        }
                        if s >= size {
                            score += 1;
                            break;
                        }
                    }
                    score
                };

                let to_the_left = {
                    let mut score = 0;
                    for k in 0..j {
                        let (s, _, _) = self.get(i, j - k - 1);
                        if s < size {
                            score += 1;
                        }
                        if s >= size {
                            score += 1;
                            break;
                        }
                    }
                    score
                };

                let to_the_bottom = {
                    let mut score = 0;
                    for k in (i + 1)..self.height {
                        let (s, _, _) = self.get(k, j);
                        if s < size {
                            score += 1;
                        }
                        if s >= size {
                            score += 1;
                            break;
                        }
                    }
                    score
                };

                let to_the_top = {
                    let mut score = 0;
                    for k in 0..i {
                        let (s, _, _) = self.get(i - k - 1, j);
                        if s < size {
                            score += 1;
                        }
                        if s >= size {
                            score += 1;
                            break;
                        }
                    }

                    score
                };

                self.get_mut(i, j).2 = to_the_right * to_the_left * to_the_bottom * to_the_top;
            }
        }
        self.data
            .iter()
            .fold(0, |acc, (_, _, s)| if s > &acc { *s } else { acc })
    }

    fn get(&self, height: usize, width: usize) -> &(u32, bool, usize) {
        &self.data[height * self.width + width]
    }

    fn get_mut(&mut self, height: usize, width: usize) -> &mut (u32, bool, usize) {
        &mut self.data[height * self.width + width]
    }

    fn from_left<'a>(&'a mut self, index: usize) -> Slice<'a> {
        Slice::FromLeft(
            self.data
                .iter_mut()
                .skip(index * self.width)
                .take(self.width),
        )
    }

    fn from_right<'a>(&'a mut self, index: usize) -> Slice<'a> {
        Slice::FromRight(
            self.data
                .iter_mut()
                .skip(index * self.width)
                .take(self.width)
                .rev(),
        )
    }

    fn from_top<'a>(&'a mut self, index: usize) -> Slice<'a> {
        Slice::FromTop(self.data.iter_mut().skip(index).step_by(self.width))
    }

    fn from_bottom<'a>(&'a mut self, index: usize) -> Slice<'a> {
        Slice::FromBottom(self.data.iter_mut().skip(index).step_by(self.width).rev())
    }
}

impl FromStr for Input {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut height = 0;
        let mut data = vec![];

        for line in s.lines() {
            for c in line.chars() {
                data.push((
                    c.to_digit(10).ok_or_else(|| anyhow!("Invalid input"))?,
                    false,
                    0,
                ));
            }
            height += 1;
        }

        Ok(Self {
            width: data.len() / height,
            height,
            data,
        })
    }
}

impl Display for Input {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for i in 0..self.height {
            for j in 0..self.width {
                write!(f, "{},", self.data[i * self.width + j].1)?
            }
            writeln!(f)?;
        }

        Ok(())
    }
}

enum Slice<'a> {
    FromLeft(Take<Skip<IterMut<'a, (u32, bool, usize)>>>),
    FromRight(Rev<Take<Skip<IterMut<'a, (u32, bool, usize)>>>>),
    FromTop(StepBy<Skip<IterMut<'a, (u32, bool, usize)>>>),
    FromBottom(Rev<StepBy<Skip<IterMut<'a, (u32, bool, usize)>>>>),
}

impl<'a> Slice<'a> {
    pub fn ex0(self) {
        let mut max = 0;
        let util = |(size, visibility): (&u32, &mut bool), max: &mut u32| {
            if *size > *max {
                *max = *size;
                *visibility = true;
            }
        };

        match self {
            Self::FromLeft(iter) => {
                for (size, visibility, _) in iter {
                    util((size, visibility), &mut max);
                }
            }
            Self::FromRight(iter) => {
                for (size, visibility, _) in iter {
                    util((size, visibility), &mut max);
                }
            }
            Self::FromTop(iter) => {
                for (size, visibility, _) in iter {
                    util((size, visibility), &mut max);
                }
            }
            Self::FromBottom(iter) => {
                for (size, visibility, _) in iter {
                    util((size, visibility), &mut max);
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn exercice_0() {
        assert_eq!(Input::from_str(include_str!("test")).unwrap().ex0(), 21);
    }

    #[test]
    fn exercice_1() {
        assert_eq!(Input::from_str(include_str!("test")).unwrap().ex1(), 8);
    }
}
