use anyhow::{bail, Error, Result};
use std::{collections::HashMap, fmt::Display, str::FromStr};

fn main() -> Result<()> {
    println!("Ex0: {}", include_str!("data").parse::<Input>()?.ex0());
    println!("Ex1: {}", include_str!("data").parse::<Input>()?.ex1());
    Ok(())
}

#[derive(Debug)]
struct Input {
    min: Coordinate,
    max: Coordinate,
    data: HashMap<Coordinate, Element>,
}

#[derive(Debug)]
enum Element {
    Rock,
    Sand,
}

impl Input {
    pub fn ex0(&mut self) -> usize {
        let mut score = 0;

        loop {
            let mut sand = Coordinate(0, 500);

            while sand.0 <= self.max.0 {
                let down = Coordinate(sand.0 + 1, sand.1);
                let left = Coordinate(sand.0 + 1, sand.1 - 1);
                let right = Coordinate(sand.0 + 1, sand.1 + 1);

                if self.data.get(&down).is_none() {
                    sand = down;
                } else if self.data.get(&left).is_none() {
                    sand = left;
                } else if self.data.get(&right).is_none() {
                    sand = right;
                } else {
                    if self.min.0 > sand.0 {
                        self.min.0 = sand.0;
                    }

                    self.data.insert(sand.clone(), Element::Sand);
                    break;
                };
            }

            if sand.0 >= self.max.0 {
                break;
            }

            score += 1;
        }

        score
    }

    pub fn ex1(&mut self) -> usize {
        let mut score = 0;

        while self.data.get(&Coordinate(0, 500)).is_none() {
            let mut sand = Coordinate(0, 500);

            loop {
                let down = Coordinate(sand.0 + 1, sand.1);
                let left = Coordinate(sand.0 + 1, sand.1 - 1);
                let right = Coordinate(sand.0 + 1, sand.1 + 1);

                if self.data.get(&down).is_none() {
                    sand = down;
                } else if self.data.get(&left).is_none() {
                    sand = left;
                } else if self.data.get(&right).is_none() {
                    sand = right;
                } else {
                    self.data.insert(sand.clone(), Element::Sand);
                    break;
                };

                if sand.0 == self.max.0 + 1 {
                    self.data.insert(sand.clone(), Element::Sand);
                    break;
                }
            }
            score += 1;
        }

        score
    }
}

impl FromStr for Input {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut data = HashMap::new();
        let mut min = Coordinate(0, usize::MAX);
        let mut max = Coordinate(usize::MIN, usize::MIN);

        for line in s.lines() {
            let mut line = line
                .split(" -> ")
                .map(|s| s.parse::<Coordinate>().unwrap())
                .peekable();

            while let Some(start) = line.next() {
                if let Some(end) = line.peek() {
                    for row in start.0.min(end.0)..(start.0.max(end.0) + 1) {
                        for col in start.1.min(end.1)..(start.1.max(end.1) + 1) {
                            {
                                if row > max.0 {
                                    max.0 = row;
                                }
                                if col > max.1 {
                                    max.1 = col;
                                }
                                if col < min.1 {
                                    min.1 = col;
                                }
                            }
                            data.insert(Coordinate(row, col), Element::Rock);
                        }
                    }
                }
            }
        }

        Ok(Self { data, min, max })
    }
}

impl Display for Input {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Min: {:?}, Max: {:?}", self.min, self.max)?;
        for row in 0..self.max.0 + 2 {
            for col in self.min.1 - 5..self.max.1 + 5 + 1 {
                if let Some(element) = self.data.get(&Coordinate(row, col)) {
                    write!(f, "{}", element)?;
                } else {
                    write!(f, " ")?;
                }
            }
            writeln!(f)?;
        }

        for _ in self.min.1 - 5..self.max.1 + 5 + 1 {
            write!(f, "#")?;
        }

        Ok(())
    }
}

impl Display for Element {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Rock => '#',
                Self::Sand => '*',
            }
        )
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct Coordinate(usize, usize);

impl FromStr for Coordinate {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let Some((lhs, rhs)) = s.split_once(',') else { bail!("Failed to parse Coordinate") };
        Ok(Self(rhs.parse()?, lhs.parse()?))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() -> Result<()> {
        assert_eq!(include_str!("test").parse::<Input>()?.ex0(), 24);
        assert_eq!(include_str!("test").parse::<Input>()?.ex1(), 93);
        Ok(())
    }
}
