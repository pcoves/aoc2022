use anyhow::{bail, Error, Result};
use std::{
    cmp::Ordering,
    fmt::Display,
    iter::Peekable,
    str::{Chars, FromStr, Lines},
};

#[derive(Debug)]
struct Input(Vec<Packets>);

#[derive(Debug)]
struct Packets(Data, Data);

#[derive(Debug, Clone, PartialEq, Eq, Ord)]
enum Data {
    Int(u8),
    List(Vec<Data>),
}

impl Input {
    pub fn ex0(&self) -> usize {
        self.0
            .iter()
            .map(|Packets(lhs, rhs)| lhs <= rhs)
            .enumerate()
            .fold(
                0,
                |acc, (index, order)| if order { acc + index + 1 } else { acc },
            )
    }

    pub fn ex1(&self) -> usize {
        let Packets(lhs, rhs) = Packets::dividers();

        let mut packets = self.0.iter().fold(
            vec![lhs.clone(), rhs.clone()],
            |mut acc, Packets(lhs, rhs)| {
                acc.push(lhs.clone());
                acc.push(rhs.clone());
                acc
            },
        );
        packets.sort();

        packets
            .iter()
            .enumerate()
            .filter(|(_, packet)| **packet == lhs || **packet == rhs)
            .fold(1, |acc, (i, _)| acc * (i + 1))
    }
}

impl FromStr for Input {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut input = vec![];

        let mut lines = s.lines().peekable();
        while lines.peek().is_some() {
            input.push(Packets::new(&mut lines)?);
            lines.next(); // Skip blank line
        }

        Ok(Self(input))
    }
}

impl Packets {
    pub fn new(iter: &mut Peekable<Lines>) -> Result<Self> {
        let Some(lhs) = iter.next() else { bail!("Failed to parse Packets: missing input") };
        let Some(rhs) = iter.next() else { bail!("Failed to parse Packets: missing input") };
        Ok(Self(lhs.parse()?, rhs.parse()?))
    }

    pub fn dividers() -> Self {
        Self(
            Data::List(vec![Data::List(vec![Data::Int(2)])]),
            Data::List(vec![Data::List(vec![Data::Int(6)])]),
        )
    }
}

impl Data {
    pub fn new(iter: &mut Peekable<Chars>) -> Result<Self> {
        let data = if iter
            .peek()
            .map(|c| *c == '[')
            .expect("Failed to parse Data: missing input")
        {
            Self::list(iter)
        } else {
            Self::int(iter)
        };

        if iter.peek().map(|c| *c == ',').unwrap_or(false) {
            iter.next(); // Skip ',' following a list
        }

        data
    }

    fn list(iter: &mut Peekable<Chars>) -> Result<Self> {
        iter.next(); // Skip opening '['
        let mut list = vec![];

        while iter.peek().map(|c| *c != ']').unwrap_or(false) {
            list.push(Data::new(iter)?);
        }
        iter.next(); // Skip closing ']'

        Ok(Self::List(list))
    }

    fn int(iter: &mut Peekable<Chars>) -> Result<Self> {
        let mut acc = String::new();

        while iter.peek().map(|c| *c != ',' && *c != ']').unwrap() {
            acc.push(iter.next().unwrap());
        }

        Ok(Self::Int(acc.parse()?))
    }
}

impl FromStr for Data {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::new(&mut s.chars().peekable())
    }
}

impl PartialOrd for Data {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Self::Int(lhs), Self::Int(rhs)) => lhs.partial_cmp(rhs),
            (lhs, Self::Int(rhs)) => lhs.partial_cmp(&Self::List(vec![Self::Int(*rhs)])),
            (Self::Int(lhs), rhs) => Self::List(vec![Self::Int(*lhs)]).partial_cmp(rhs),
            (Self::List(lhs), Self::List(rhs)) => lhs
                .iter()
                .zip(rhs.iter())
                .find(|(lhs, rhs)| lhs != rhs)
                .map_or_else(
                    || lhs.len().partial_cmp(&rhs.len()),
                    |(lhs, rhs)| lhs.partial_cmp(rhs),
                ),
        }
    }
}

impl Display for Input {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for packets in &self.0 {
            writeln!(f, "{}", packets.0)?;
            writeln!(f, "{}", packets.1)?;
            writeln!(f)?;
        }

        Ok(())
    }
}

impl Display for Packets {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}, {}", self.0, self.1)
    }
}

impl Display for Data {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Int(int) => write!(f, "{}", int),
            Self::List(list) => {
                write!(f, "[")?;
                let mut list = list.iter().peekable();
                while let Some(l) = list.next() {
                    write!(f, "{}", l)?;
                    if list.peek().is_some() {
                        write!(f, ",")?;
                    }
                }
                write!(f, "]")
            }
        }
    }
}

fn main() -> Result<()> {
    let input: Input = include_str!("data").parse()?;
    println!("Ex0: {}", input.ex0());
    println!("Ex1: {}", input.ex1());
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() -> Result<()> {
        assert_eq!(include_str!("test").parse::<Input>()?.ex0(), 13);
        assert_eq!(include_str!("test").parse::<Input>()?.ex1(), 140);
        Ok(())
    }
}
