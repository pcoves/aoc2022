use anyhow::{Context, Error, Result};
use std::str::FromStr;

pub fn day4() -> Result<()> {
    println!("Ex0: {}", ex0(include_str!("data")));
    println!("Ex1: {}", ex1(include_str!("data")));
    Ok(())
}

fn ex0(file: &str) -> usize {
    file.lines()
        .map(|line| {
            let pair = Pair::from_str(line).unwrap();
            let contains = pair.contains();
            contains
        })
        .filter(|c| *c)
        .collect::<Vec<_>>()
        .len()
}

fn ex1(file: &str) -> usize {
    file.lines()
        .map(|line| {
            let pair = Pair::from_str(line).unwrap();
            let contains = pair.overlaps();
            contains
        })
        .filter(|c| *c)
        .collect::<Vec<_>>()
        .len()
}

struct Pair {
    lhs: Elf,
    rhs: Elf,
}

impl FromStr for Pair {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (lhs, rhs) = s.split_once(',').with_context(|| "Failed to read Pair")?;
        Ok(Self {
            rhs: Elf::from_str(lhs)?,
            lhs: Elf::from_str(rhs)?,
        })
    }
}

impl Pair {
    pub fn contains(&self) -> bool {
        (self.lhs.min <= self.rhs.min && self.lhs.max >= self.rhs.max)
            || (self.rhs.min <= self.lhs.min && self.rhs.max >= self.lhs.max)
    }

    pub fn overlaps(&self) -> bool {
        self.contains()
            || (self.lhs.min >= self.rhs.min && self.lhs.min <= self.rhs.max)
            || (self.lhs.max <= self.rhs.min && self.lhs.max >= self.rhs.max)
            || (self.rhs.min >= self.lhs.min && self.rhs.min <= self.lhs.max)
            || (self.rhs.max <= self.lhs.min && self.rhs.max >= self.lhs.max)
    }
}

struct Elf {
    min: u32,
    max: u32,
}

impl FromStr for Elf {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (min, max) = s.split_once('-').with_context(|| "Failed to read Elf")?;
        Ok(Self {
            min: min.parse::<u32>()?,
            max: max.parse::<u32>()?,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn exercice_0() {
        assert_eq!(ex0(include_str!("test")), 2);
    }

    #[test]
    fn exercice_1() {
        assert_eq!(ex1(include_str!("test")), 4);
    }
}
